import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class LoginPage extends BaseTest {

    @Test
    public void validCredentials () throws InterruptedException {

        Thread.sleep (5000);
        driver.findElement (By.xpath ("//input[@placeholder='Username']")).sendKeys ("Admin");
        driver.findElement (By.xpath ("//input[@placeholder='Password']")).sendKeys ("admin123");
        driver.findElement (By.xpath ("//button[@type='submit']")).click ();
        Thread.sleep (5000);
        String newPageText = driver.findElement (By.xpath ("//div[@class='oxd-topbar-header-title']")).getText ();
        System.out.println ("newPageText :" + newPageText);
        Assert.assertEquals (newPageText , "Dashboard");

    }

}