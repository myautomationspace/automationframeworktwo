import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class ForgotPasswordPage extends BaseTest {

    @Test
    public void getHeading () throws InterruptedException {

        Thread.sleep (3000);
        WebElement ele = driver.findElement (By.xpath ("//p[text()='Forgot your password? ']"));
        Assert.assertEquals (ele.getText ().trim () , "Forgot your password?");
        ele.click ();
        Thread.sleep (2000);
        String heading = driver.findElement (By.xpath ("//h6[text()='Reset Password']")).getText ();
        System.out.println ("Title : " + heading);
        Assert.assertEquals (heading , "Reset Password");
    }

}