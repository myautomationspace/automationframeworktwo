import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LogOut  extends  BaseTest{
    @Test
    public void logout() throws InterruptedException {
        Thread.sleep (5000);
        driver.findElement (By.xpath ("//input[@placeholder='Username']")).sendKeys ("Admin");
        driver.findElement (By.xpath ("//input[@placeholder='Password']")).sendKeys ("admin123");
        driver.findElement (By.xpath ("//button[@type='submit']")).click ();
        Thread.sleep (5000);
        String newPageText = driver.findElement (By.xpath ("//div[@class='oxd-topbar-header-title']")).getText ();
        System.out.println ("newPageText :" + newPageText);
        Assert.assertEquals (newPageText , "Dashboard");

        Thread.sleep (2000);
        WebElement ele = driver.findElement (By.xpath ("//span[@class='oxd-userdropdown-tab']"));
        ele.click ();
        Thread.sleep (2000);
        WebElement logOutEle = driver.findElement (By.xpath ("//a[contains(text(),'Logout')]"));
        logOutEle.click ();

    }
}
